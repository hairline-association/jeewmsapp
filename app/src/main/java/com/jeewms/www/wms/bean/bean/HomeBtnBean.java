package com.jeewms.www.wms.bean.bean;

/**
 * Created by 13799 on 2018/6/2.
 */

public class HomeBtnBean {
    private String btnName;
    private int imaResId;
    private String type;

    public String getBtnName() {
        return btnName;
    }

    public void setBtnName(String btnName) {
        this.btnName = btnName;
    }

    public int getImaResId() {
        return imaResId;
    }

    public void setImaResId(int imaResId) {
        this.imaResId = imaResId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public HomeBtnBean(String btnName, int imaResId,String type){
        this.btnName=btnName;
        this.imaResId=imaResId;
        this.type=type;
    }
}
