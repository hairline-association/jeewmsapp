package com.jeewms.www.wms.volley;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.jeewms.www.wms.LoginActivity;
import com.jeewms.www.wms.R;
import com.jeewms.www.wms.base.BaseActivity;
import com.jeewms.www.wms.bean.bean.HomeBtnBean;
import com.jeewms.www.wms.constance.Constance;
//import com.jeewms.www.wms.ui.acitivity.Matnr_Activity;
//import com.jeewms.www.wms.ui.acitivity.Matnrth_Activity;
import com.jeewms.www.wms.ui.adapter.HomeGridAdapter;
import com.jeewms.www.wms.util.BannerUtil;
import com.jeewms.www.wms.util.ClickFastUtil;
import com.jeewms.www.wms.util.SharedPreferencesUtil;
import com.jeewms.www.wms.util.ToastUtil;
import com.zhy.android.percent.support.PercentLinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 13799 on 2018/7/2.
 */

public class HomeActivity extends BaseActivity {

    HomeGridAdapter adapter;
    GridView gvHome;
    ArrayList<HomeBtnBean> list = new ArrayList<>();
    ArrayList<HomeBtnBean> listTop = new ArrayList<>();
    List<String> bannerList = new ArrayList<>();
    @BindView(R.id.convenientBanner)
    ConvenientBanner convenientBanner;
    @BindView(R.id.shaomao)
    PercentLinearLayout shaomao;
    @BindView(R.id.renwu)
    PercentLinearLayout renwu;
    @BindView(R.id.tongji)
    PercentLinearLayout tongji;
    @BindView(R.id.genduo)
    PercentLinearLayout genduo;
    @BindView(R.id.sl_hold)
    ScrollView slHold;
    @BindView(R.id.indexView)
    ImageView indexView;

    public static void show(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home2);
        ButterKnife.bind(this);
        init();
        setBanner();
    }

    private void init() {
        findViewById(R.id.btn_titlebar_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferencesUtil.getInstance(HomeActivity.this).setKeyValue(Constance.SHAREP.PASSWORD, "");
                LoginActivity.show(HomeActivity.this);
                finish();
            }
        });
        adapter = new HomeGridAdapter();
        gvHome = findViewById(R.id.gv_home);
        gvHome.setAdapter(adapter);
        gvHome.setFocusable(false);
        ((TextView)findViewById(R.id.btn_titlebar_left)).setText("v3.1用户:"+SharedPreferencesUtil.getInstance(this).getKeyValue(Constance.SHAREP.LOGINNAME));        addBtn();
      //  ((TextView)findViewById(R.id.btn_titlebar_left)).setTextColor(96);

    }

    //添加按钮
    private void addBtn() {
        list.clear();
        for (int i = 0; i < Constance.btnNameList.length; i++) {
            addBtn2List(Constance.btnNameList[i], Constance.btnImgList[i],Constance.btnTypeList[i]);
        }
        list.add(new HomeBtnBean("", 0,""));
        adapter.setList(list);
        adapter.notifyDataSetChanged();
    }

    private void addBtn2List(String btnName, int imgResId,String type) {
        HomeBtnBean btn1 = new HomeBtnBean(btnName, imgResId,type);
        list.add(btn1);
    }

    public void setBanner() {
      //  bannerList.add("http://192.168.200.100/zzwms/images/app/home1.jpg");
        bannerList.add("http://120.78.150.43/pic/home1.jpg");
      //  bannerList.add("http://192.168.200.100/zzwms/images/app/home3.jpg");
        BannerUtil.showBanner(convenientBanner, bannerList);
    }

    @OnClick({R.id.shaomao, R.id.renwu, R.id.tongji, R.id.genduo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.shaomao:
            //    if(ClickFastUtil.isFastClick3000())
              //  ToastUtil.show(this, "功能暂未开放！");
//                Intent intent = new Intent(HomeActivity.this,Matnr_Activity.class);
//                startActivity(intent);
                ToastUtil.show(this, "功能暂未开放！");

                break;
            case R.id.renwu:
//                Intent intentth = new Intent(HomeActivity.this, Matnrth_Activity.class);
//                startActivity(intentth);
                ToastUtil.show(this, "功能暂未开放！");

                break;
            case R.id.tongji:
                //  ToastUtil.show(this, "功能暂未开放！");
                ToastUtil.show(this, "V5.0！");
                break;

//                ToastUtil.show(this, "功能暂未开放！");
//                break;
            case R.id.genduo:
                ToastUtil.show(this, "功能暂未开放！");
                break;
        }
    }
}
