package com.jeewms.www.wms.test;

import com.jeewms.www.wms.bean.vm.CollectGoodsVm;
import com.jeewms.www.wms.ui.adapter.CollectGoodsAdapter;
import com.jeewms.www.wms.util.LoadingUtil;

import java.util.ArrayList;
import java.util.List;

public class TestDate {


    public static void getCollectTestDate(CollectGoodsAdapter mAdapter){
        mAdapter.seCollectGoodsVmList(getCollectDate());
        mAdapter.notifyDataSetChanged();
    }



    private static List<CollectGoodsVm>  getCollectDate(){
        List<CollectGoodsVm> list=new ArrayList<>();
        list.add(getCollectGoodsVm());
        list.add(getCollectGoodsVm());
        list.add(getCollectGoodsVm());
        list.add(getCollectGoodsVm());
        return list;
    }

    private static CollectGoodsVm getCollectGoodsVm(){
        CollectGoodsVm vm=new CollectGoodsVm();
        vm.setCfWenCeng("");
        vm.setId("");
        return vm;
    }



}
